import express from "express";
import bodyParser from "body-parser";
import { graphqlExpress, graphiqlExpress } from "apollo-server-express";
import { makeExecutableSchema } from "graphql-tools";
import path from "path";
import { fileLoader, mergeTypes, mergeResolvers } from "merge-graphql-schemas";
import cors from "cors";
import jwt from "jsonwebtoken";
import _ from "./.env.js";

import models from "./models";
import { refreshTokens } from "./auth";

const SECRET = process.env.SECRET;
const SECRET2 = process.env.SECRET2;
const typeDefs = mergeTypes(fileLoader(path.join(__dirname, "./schema")));
const resolvers = mergeResolvers(
  fileLoader(path.join(__dirname, "./resolvers"))
);

const schema = makeExecutableSchema({
  typeDefs,
  resolvers
});

const app = express();

app.use(cors("*"));

const addUser = async (req, res, next) => {
  const token = req.headers["x-token"];
  console.log(token);
  if (token) {
    try {
      const { user } = jwt.verify(token, SECRET);
      req.user = user;
      console.log(`Found user: ${user}`);
    } catch (err) {
      const refreshToken = req.headers["x-refresh-token"];
      const newTokens = await refreshTokens(
        token,
        refreshToken,
        models,
        SECRET
      );
      if (newTokens.token && newTokens.refreshToken) {
        console.log("Refreshing tokens!");
        req.set("AccessControl-Expose-Headers", "x-token, x-refresh-token");
        req.set("x-token", newTokens.token);
        req.set("x-refresh-token", newTokens.refreshToken);
      }
    }
  }

  next();
};

app.use(addUser);

app.use(
  "/graphql",
  bodyParser.json(),
  graphqlExpress(req => ({
    schema,
    context: {
      models,
      SECRET,
      SECRET2,
      user: req.user
    }
  }))
);
app.use("/graphiql", graphiqlExpress({ endpointURL: "/graphql" }));

models.sequelize
  .sync({
    /* force: true */
  })
  .then(() => {
    app.listen(8081);
  });
