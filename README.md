# Slack Clone Server

## Introduction

This is based on a [tutorial](https://www.youtube.com/watch?v=0MKJ7JbVnFc&list=PLN3n1USn4xlkdRlq3VZ1sT6SGW0-yajjL) from [Ben Awad](https://twitter.com/benawad97). His tutorial is top notch. Check out the [client portion](https://gitlab.com/jbydeley/slack-clone-vue) written in VueJS.

## Prerequisites

* Node
* Yarn (optional but helpful)
* Postgres

## Installation

1.  Create a database in Postgres
1.  Move `.env.example` to `.env` and add your postgres username, password, database. 
1.  Add a secret and secret2 to the `.env`. This can be anything at all.
1.  `yarn` or `npm install`
1.  `yarn start` or `npm start`
