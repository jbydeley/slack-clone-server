export default (sequelize, DataTypes) => {
  const Team = sequelize.define("team", {
    name: {
      type: DataTypes.STRING,
      unique: true,
      validate: {
        len: {
          args: [1, 100],
          msg: "The team name needs to be between 1 and 100 characters long"
        }
      }
    }
  });

  Team.associate = models => {
    Team.belongsToMany(models.User, {
      through: "member",
      foreignKey: {
        name: "teamId",
        field: "team_id"
      }
    });

    Team.belongsTo(models.User, {
      foreignKey: "owner"
    });
  };

  return Team;
};
